require "flickraw"
require "yaml"
require "date"
require "mechanize"

#config info for Flickr
config = YAML.load_file("config.yml")
API_KEY = config['API_KEY']
API_SECRET = config['API_SECRET']


class FlickSearch
  attr_accessor :search_term
  def initialize search_term
    #register the API info
	  FlickRaw.api_key = API_KEY
	  FlickRaw.shared_secret = API_SECRET
 	  @search_term = search_term
 	  @today = Date.today
	  @yesterday = @today - 1
	  @two_days_ago = @yesterday - 1
    @args = {
      :tags => @search_term, 
		  :min_taken_date => @two_days_ago, 
		  :max_taken_date => @today,
		  :license => "2"
		  }
  end
  
  def get_photo
    search = flickr.photos.search(@args)
    puts search.length
	  if search.length > 0
      sizes = flickr.photos.getSizes(:photo_id => search.first.id)
		  medium = sizes.find{ |size| size.label == "Medium"}
		  #visit link and download image
		  puts medium.source
		  browser = Mechanize.new
		  browser.get(medium.source).save("images/#{@args[:tags]}.jpg")
	  end
  end	  
end
	
my_search = FlickSearch.new 'flower'
my_search.get_photo
