import flickrapi
import datetime
import yaml
import requests

today = datetime.date.today()
yesterday = today - datetime.timedelta(days=1)

with open('config.yml') as f:
    config = yaml.load(f)

flickr = flickrapi.FlickrAPI(config['API_KEY'], config['API_SECRET'])
search = flickr.photos_search(min_taken_date=today, tags='flower', license='2')
first_id = search[0][0].get('id')
info = flickr.photos_getSizes(photo_id=first_id)
sizes = info.findall('sizes')
for size in sizes:
    for node in size:
        if node.get('label') == "Medium":
            source = node.get('source')
r = requests.get(source)
f = open('images/download.jpg', 'wb')
f.write(r.content)
f.close()

